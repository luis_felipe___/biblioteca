package pucrs.progoo;

public class Usuario {
	private String codigo;
	private String nome;
	private String cpf;
	
	public Usuario(String nome, String cpf, String codigo){
		this.nome = nome;
		this.codigo = codigo;
		this.cpf = cpf;	
	}
		public String getNome(){
			return nome;
		}
		public String getCpf(){
			return cpf;
		}
		public String getCodigo(){
			return codigo;
		}
		public void setNome(String nome){
			this.nome = nome;
		}
		public void setCpf(String cpf){
			this.cpf = cpf;
		}
		public void setCodigo(String codigo){
			this.codigo = codigo;
		}				
	}		
	
