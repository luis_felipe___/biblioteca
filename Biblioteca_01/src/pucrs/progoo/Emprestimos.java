package pucrs.progoo;

import java.util.ArrayList;

public class Emprestimos {

	private ArrayList<Emprestimo> emprestimos;

	public Emprestimos(ArrayList<Emprestimo> emprestimos) {
		
		this.emprestimos = emprestimos;
	}

	public ArrayList<Emprestimo> getEmprestimos() {
		return emprestimos;
	}

	public void setEmprestimos(ArrayList<Emprestimo> emprestimos) {
		this.emprestimos = emprestimos;
	}

}
