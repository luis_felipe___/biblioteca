package pucrs.progoo;

import java.util.ArrayList;

public class Acervo {

	public Acervo(ArrayList<Livro> acervo) {
		
		this.acervo = acervo;
	}

	private ArrayList<Livro> acervo;

	public ArrayList<Livro> getAcervo() {
		return acervo;
	}

	public void setAcervo(ArrayList<Livro> acervo) {
		this.acervo = acervo;
	}

}
