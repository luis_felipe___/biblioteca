package pucrs.progoo;

import java.util.ArrayList;

public class Cadastro {
	private ArrayList<Usuario> usuarios;

	public Cadastro(ArrayList<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	public ArrayList<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(ArrayList<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	public boolean cadastrar(String codigo, String nome, String cpf) {
		Usuario n1 = new Usuario(nome, cpf, codigo);
		return usuarios.add(n1);
	}
	public Usuario buscarUsuario(String nome){		
		for(Usuario aux: usuarios){
			if(aux.getNome().equals(nome)){
				return aux;
			}			
		}
		return null;
	}
	
}
